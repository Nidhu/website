from flask import Flask, render_template
from root import Root
from download import Download

app      = Flask(__name__)
root     = Root()
download = Download()

@app.route("/")
def getRoot():
    return root._exec();

@app.route("/download")
def getDownload():
    return download._exec();

if __name__ == "__main__":
    app.run(debug = True);


    
